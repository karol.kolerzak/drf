from django.db import models
from django.contrib.auth.models import AbstractBaseUser
from django.utils import timezone
from .menager import CustomUserMenager
from django_resized import ResizedImageField




class CustomUser(AbstractBaseUser):

    BASIC = 'BASIC'
    PREMIUM = 'PREMIUM'
    ENTERPRISE = 'ENTERPRISE'
    ACCOUNT_CHOICES = [
        (BASIC, 'Basic'),
        (PREMIUM, 'Premium'),
        (ENTERPRISE, 'Enterprise'),
    ]

    username = models.CharField(max_length=150, unique=True)
    account_type = models.CharField(max_length=10, choices=ACCOUNT_CHOICES, blank=False)
    is_active = models.BooleanField(default=True)
    is_admin = models.BooleanField(default=False)
    is_staff = models.BooleanField(default=False)
    date_joined = models.DateTimeField(default=timezone.now)

    object = CustomUserMenager()

    USERNAME_FIELD = 'username'

    def __str__(self):
        return self.username

    def has_perm(self, perm, obj=None):
        "Does the user have a specific permission?"
        # Simplest possible answer: Yes, always
        return True

    def has_module_perms(self, app_label):
        "Does the user have permissions to view the app `app_label`?"
        # Simplest possible answer: Yes, always
        return True


class Image(models.Model):
    user = models.ForeignKey(CustomUser, on_delete=models.CASCADE)
    title = models.CharField(max_length=100)
    image = models.ImageField(upload_to='images/', blank=False)
    date = models.DateTimeField(auto_now_add=True)
    image200 = ResizedImageField(size=[200, 200], blank=True, null=True)
    image400 = ResizedImageField(size=[400, 400], blank=True, null=True)


    def __str__(self):
        return self.title