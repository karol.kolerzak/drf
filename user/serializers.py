from django.contrib.auth import get_user_model
from rest_framework import serializers
from .models import Image

User = get_user_model()


class RegisterUserSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = ['id', 'username', 'password', 'account_type']
        extra_kwargs = {
            'password': {'write_only': True}
        }

        def create(self, validate_data):
            return User.objects.create_user(**validate_data)


class ImageSerializerBasic(serializers.ModelSerializer):

    class Meta:
        model = Image
        fields = ['id', 'user', 'title', 'date', 'image200']


class ImageSerializerEnterprise(ImageSerializerBasic):

    class Meta:
        model = Image
        fields = ImageSerializerBasic.Meta.fields + ['image400', ]


class ImageSerializerPremium(serializers.ModelSerializer):

    class Meta:
        model = Image
        fields = ['id', 'user', 'title', 'date', 'image', 'image200', 'image400']


