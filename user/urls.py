from django.urls import path, include
from rest_framework.authtoken.views import obtain_auth_token
from .views import RegisterUserView, ImageView
from rest_framework import routers

app_name = 'users'

router = routers.DefaultRouter()
router.register('image', ImageView)

urlpatterns = [
    path('api-token-auth/', obtain_auth_token, name='login'),
    path('register/', RegisterUserView.as_view(), name='register'),
    path('', include(router.urls)),


]