from django.contrib.auth.models import BaseUserManager



class CustomUserMenager(BaseUserManager):

    def create_user(self, username, password=None, account_type="BASIC", **extra_fields):
        if not username:
            raise ValueError("Users must have a login")

        user = self.model(username=username, account_type=account_type, **extra_fields)
        user.set_password(password)
        user.save(using=self._db)
        return user

    def create_superuser(self, username, password=None, account_type="ENTERPRISE", **extra_fields):
        if not username:
            raise ValueError("Users must have a login")

        user = self.model(username=username, account_type=account_type, **extra_fields)
        user.set_password(password)
        user.is_staff = True
        user.is_admin = True
        user.save(using=self._db)
        return user
