from rest_framework import generics, permissions, viewsets
from .serializers import RegisterUserSerializer, ImageSerializerBasic, ImageSerializerPremium, ImageSerializerEnterprise
from .models import Image
from rest_framework.response import Response



class RegisterUserView(generics.CreateAPIView):
    serializer_class = RegisterUserSerializer
    permission_classes = (permissions.AllowAny, )


class ImageView(viewsets.ModelViewSet):
    queryset = Image.objects.all()
    serializer_class = ImageSerializerBasic

    def create(self, request, *args, **kwargs):
        data = request.data
        image = Image.objects.create(user=request.user,
            title=data['title'], image=data['image'], image200=data['image'], image400=data['image']
        )
        image.save()

        serializer = ImageSerializerPremium(image)
        return Response(serializer.data)

    def retrieve(self, request, *args, **kwargs):
        user_type = request.user.account_type
        image = self.get_object()
        if user_type == 'BASIC':
            serializer = ImageSerializerBasic(image)
        elif user_type == 'ENTERPRISE':
            serializer = ImageSerializerEnterprise(image)
        elif user_type == 'PREMIUM':
            serializer = ImageSerializerPremium(image)

        return Response(serializer.data)

    def list(self, request, *args, **kwargs):
        user_type = request.user.account_type
        if user_type == 'BASIC':
            image = Image.objects.all()
            serializer = ImageSerializerBasic(image, many=True)
            return Response(serializer.data)

        elif user_type == 'ENTERPRISE':
            image = Image.objects.all()
            serializer = ImageSerializerEnterprise(image, many=True)
            return Response(serializer.data)

        elif user_type == 'PREMIUM':
            image = Image.objects.all()
            serializer = ImageSerializerPremium(image, many=True)
            return Response(serializer.data)




