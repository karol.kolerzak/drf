
from django.contrib import admin
from django.urls import path, include
from django.conf.urls.static import static
from django.conf import settings

from rest_framework import routers


# router = routers.DefaultRouter()
# router.register(r'customers', CustomerViewSet)
# router.register(r'profession', ProfessionViewSet)
# router.register(r'datasheet', DataSheetViewSet)
# router.register(r'documents', DocumentViewSet)
#

urlpatterns = [
    path('admin/', admin.site.urls),
    path('user/', include('user.urls')),
] + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
